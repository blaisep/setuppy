Welcome to setuppy's documentation!
===================================

A project for besting python in packaging.

.. only:: html

    The latest version of this document is also available as |pdflink|.


.. toctree::
   :maxdepth: 2
   :caption: Contents:

   about
   modules

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
