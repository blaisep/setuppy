from pathlib import Path

import pytest


@pytest.fixture()
def project_root_dir():
    dir = Path(__file__).parent

    while dir:
        if list(dir.glob('setup.cfg')):
            return dir
        dir = dir.parent

    raise Exception('You break something badly')
