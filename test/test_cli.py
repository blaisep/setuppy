from click.testing import CliRunner


from setuppy import cli


def test_hello_world():
    runner = CliRunner()
    result = runner.invoke(cli.main, ['Peter'])
    assert result.exit_code == 0
    assert 'Hello Peter!\n' in result.output
