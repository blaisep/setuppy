import configparser

import pytest


def test_all_extra_contains_all_extras(setup_config):
    extras = setup_config['options.extras_require']

    all_extras = set()
    expected = set()

    for extra_name, dependencies in extras.items():
        if extra_name == 'all':
            all_extras.update(dependencies.strip().splitlines())
        else:
            expected.update(dependencies.strip().splitlines())

    assert all_extras == expected


@pytest.fixture()
def setup_config(project_root_dir):
    config = configparser.ConfigParser()
    config.read(str(project_root_dir / 'setup.cfg'))
    return config
