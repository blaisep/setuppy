import json
from pathlib import Path

import pytest


class Project(object):
    def __init__(self, cookies, context):
        self.cookies = cookies
        self.context = context
        self.result = cookies.bake()

        self.path = Path(str(self.result.project))
        assert self.result.exit_code == 0
        assert self.result.exception is None
        assert self.result.project.isdir()


@pytest.fixture()
def defaultproject(cookies, project_root_dir):
    json_file = project_root_dir / 'cookiecutter.json'

    yield Project(
        cookies,
        json.loads(json_file.open().read())
    )
