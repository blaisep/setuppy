import textwrap


def test_has_gitlab_README(defaultproject):
    qualifying_filenames = {
        defaultproject.path / 'README',
        defaultproject.path / 'README.txt',
        defaultproject.path / 'README.md',
        defaultproject.path / 'README.rst'
    }

    # & is intersection of sets
    qualifying_paths = qualifying_filenames & set(
                       defaultproject.path.glob('*'))

    assert len(qualifying_paths) == 1, textwrap.dedent("""
    Should have had one and only one (1) qualifying README file.
    """.format(', '.join(str(filename) for filename in qualifying_filenames)))

    readme_path = qualifying_paths.pop()
    readme_text = readme_path.open().read()

    assert defaultproject.context['project_name'] in readme_text
