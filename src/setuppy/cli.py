import sys

import click

from setuppy import __dist__


@click.command()
@click.version_option(version=__dist__.version)
@click.argument('name')
def main(name):
    if sys.version_info < (3, 4):
        click.Abort('Python 3.4+ is required')
    if sys.version_info < (3, 5):
        click.echo('Python 3.4 detected', err=True)
    if sys.version_info < (3, 6):
        click.echo('Python 3.5 detected', err=True)
    if sys.version_info < (3, 7):
        click.echo('Python 3.6 detected', err=True)
    click.echo('Hello {!s}!'.format(name))
